import React from 'react';
import Home from './components/Home';
import NotFound from './components/NotFound';
import {BrowserRouter, Switch, Route} from 'react-router-dom';

const App = () => {
  return (
      <BrowserRouter>
        <Switch>
          <Route exact path="/" component={Home}/>
          <Route exact path="/home" component={Home}/>
          <Route component={NotFound}/>
        </Switch>
      </BrowserRouter>
  );
};
export default App;