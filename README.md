HOME-TASK FROM SKYCOP
--

**Description**

You have to create a simple React/React-Redux App that "fetches" a response into Redux state, displays a dropdown with the results, and upon changing a selection, it's value (not title) has to be displayed somewhere.

**Install**

npm i - installs modules

**Run project**

npm start