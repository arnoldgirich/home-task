import React from 'react';
import ReactDOM from 'react-dom';
import styledNormalize from 'styled-normalize';
import 'typeface-architects-daughter';
import {injectGlobal} from 'styled-components';
import App from './App';
import dropdown from './redux/reducers/dropdownReducer';
import {createStore, combineReducers} from 'redux';
import {Provider} from 'react-redux';

const rootReducer = combineReducers({dropdown});

const store = createStore(
    rootReducer,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

ReactDOM.render(
    <Provider store={store}>
      <App/>
    </Provider>,
    document.getElementById('root')
);

// Global styles

injectGlobal`
${styledNormalize}
body {
  text-align: center;
  font-family: 'Architects Daughter', cursive;
}
`;
