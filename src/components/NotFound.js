import React, {Fragment} from 'react';

const NotFound = () => {
  return (
      <Fragment>
        <p>Error 404, not found</p>
      </Fragment>
  )
};
export default NotFound;