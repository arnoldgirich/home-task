import * as types from '../types';
const response = require('../assets/response');

export const fetchDropdown = () => {
  return {
    type: types.FETCH_DROPDOWN,
    payload: response.payload
  }
};

export const deleteDropdown = () => {
  return {
    type: types.DELETE_DROPDOWN
  }
};