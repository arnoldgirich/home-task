import * as types from '../types';

export default (state = [], action) => {
  switch (action.type) {
    case types.FETCH_DROPDOWN:
      return action.payload;
    case types.DELETE_DROPDOWN:
      return [];
    default:
      return [];
  }
};