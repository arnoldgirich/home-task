import React, {Component, Fragment} from 'react';
import styled from 'styled-components';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import * as actions from '../redux/actions/dropdownActions';

const Button = styled.button`
  margin-top: 200px;
  border: 1px solid orange;
  background-color: #fff;
  padding: 10px 20px;
  color: orange;
  font-size: 30px;
  font-weight: 700;
  outline: none;
  cursor: pointer;
  user-select: none;
  &:hover {
    background-color: orange;
    color: #fff;
  }
  &:active {
    transform: scale(.9);
  }
  `;

const Select = styled.select`
  margin: 100px auto;
  font-size: 20px;
  padding: 10px;
  cursor: pointer;
  outline: none;
  `;

class Home extends Component {

  state = {
    active: false,
    value: 'default value'
  };

  componentDidMount() {
    this.props.history.push('/home');
  }

  handleClick = () => {
    !this.state.active
        ?
        this.props.fetchDropdown()
        :
        this.props.deleteDropdown();
    this.setState(prevState => {
      return {active: !prevState.active}
    });
  };

  handleChange = e => {
    this.setState({value: e.target.value})
  };

  render() {

    const options = this.props.dropdown.map((option, i) => (
        <option
            key={i}
            value={option.id}>
          {option.title}
        </option>
    ));

    return (
        <Fragment>
          <Button
              onClick={this.handleClick}>
            Click me !
          </Button>
          <Select
              value={this.state.value}
              onChange={this.handleChange}
              style={{display: this.state.active ? 'block' : 'none'}}>
            <option
                defaultValue={true}
                value="default value">
              Select an option
            </option>
            {options}
          </Select>
          <p
              style={{display: this.state.active ? 'block' : 'none'}}>
            {this.state.value}
          </p>
        </Fragment>
    );
  }
}

Home.propTypes = {
  dropdown: PropTypes.array.isRequired,
  fetchDropdown: PropTypes.func.isRequired,
  deleteDropdown: PropTypes.func.isRequired
};

const mapStateToProps = ({dropdown}) => ({dropdown});

export default connect(mapStateToProps, actions)(Home);